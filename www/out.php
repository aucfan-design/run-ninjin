<?php

//数値に変換
$target_score = intval($argv[1]);//送られたスコアを文字列から数字に変換
//intvalを使う時は引数を入れること！！（エラーが出る場合があるため）


$log = file('log.txt',FILE_IGNORE_NEW_LINES);//FILE_IGNORE_NEW_LINESは改行を飛ばす

rsort($log);//数字の大きい順にソートする

$top5 = array_slice($log, 0, 5);//logの1番目から5番目までの数値を返す
#var_dump($target_score);

$rank = 0;

foreach ($log as $key => $value) {
    if ($value <= $target_score) {
        $rank = $key + 1;
        #var_dump($rank);
        break;
    }
}//順位を出す

if ($rank <= 5) {
    $top5[] = $target_score;
    rsort($top5);
    array_pop($top5);
}//5位より上だった場合

#var_dump($top5);


$rank = $_GET["rank"];

print_r ("今回のスコアは".$rank."位です。");
print_r (PHP_EOL);
print_r ("TOP5は以下になります。");
print_r (PHP_EOL);
foreach ($top5 as $key => $value) {
    print_r(($key +1) ."位は".$value."点");
    print_r (PHP_EOL);
}



