# How to Use


### Mac上でのコマンド

```
$ git clone git@gitlab.com:aucfan-design/run-ninjin.git
$ cd run-ninjin/opsfiles/
$ vagrant plugin install vagrant-vbguest
$ vagrant up
$ vagrant ssh
```

### vagrant上でのコマンド

```
$ sudo ln -s /develop/run-ninjin/httpd/ninjin.conf /etc/httpd/conf.d/
$ sudo service httpd restart
```

### 正常にrestartしたらブラウザで確認

```
http://192.168.33.7
```